set t epslatex nobackground size 3.5in,3.5in

if (! exists('fing')) fing='../data/tor3gold-77350-ang.txt'
if (! exists('fin')) fin='../data/tor3-77349-ang.txt'
if (! exists('fout')) fout='bundles-diff-ratios.tex'

co1(x) = x < -pi/2 ? x+pi : x;
co2(x) = x > pi/2 ? x -pi: x;
an(x) = co2(co1(x));

fingtmp = '/tmp/kkfing__'
fintmp = '/tmp/kkfin__'
tmp = '/tmp/kk__'

system(sprintf("awk 'BEGIN{f=0;}(f|| $4==%d){print $0; f=1;}' %s > %s", 16384, fing, fingtmp))
system(sprintf("awk 'BEGIN{f=0;}(f|| $4==%d){print $0; f=1;}' %s > %s", 16384, fin, fintmp))
system(sprintf("paste %s %s > %s", fingtmp, fintmp, tmp))
system(sprintf("rm %s %s", fingtmp, fintmp))

set output fout
unset grid

epsi = 0+system(sprintf("grep eps %s | awk '{print $4}'", tmp))
NN = 0+system(sprintf("grep N %s | awk '{print $4}'", tmp))
ecc = 0+system(sprintf("grep ecc %s | awk '{print $4}'", tmp))

set key t l 

set xlabel '$\theta$

set ylabel 'difference'
set key t c

set ylabel 'ratio'
p [0:1] tmp ev :::0::0 u ($0/NN):(($1-$2) / ($3-$4)) pt 7 ps .05 lc rgb 'black' w lp not

set output
