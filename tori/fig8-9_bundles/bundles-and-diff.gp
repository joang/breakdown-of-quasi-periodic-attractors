set t epslatex nobackground size 7in,7in

if (! exists('fing')) fing='../data/tor3gold-77350-ang.txt'
if (! exists('fin')) fin='../data/tor3-77349-ang.txt'
if (! exists('fout')) fout='bundles-and-diff.tex'

co1(x) = x < -pi/2 ? x+pi : x;
co2(x) = x > pi/2 ? x -pi: x;
an(x) = co2(co1(x));

set output fout
set multiplot layout 2,2
unset grid

st fin u 1 noout
lastk = STATS_blocks-2;
array ecc[STATS_blocks-1];
array epsi[STATS_blocks-1];
array NN[STATS_blocks-1];
do for [j=0:(STATS_blocks-2)] {
 epsi[j+1] = 0+system(sprintf("grep eps %s | awk 'NR==%d{print $4}'", fin,j+1))
 NN[j+1] = 0+system(sprintf("grep N %s | awk 'NR==%d{print $4}'", fin,j+1))
 ecc[j+1] = 0+system(sprintf("grep ecc %s | awk 'NR==%d{print $4}'", fin,j+1))
}

st fing u 1 noout
lastkg = STATS_blocks-2;
array eccg[STATS_blocks-1];
array epsig[STATS_blocks-1];
array NNg[STATS_blocks-1];
do for [j=0:(STATS_blocks-2)] {
 epsig[j+1] = 0+system(sprintf("grep eps %s | awk 'NR==%d{print $4}'", fing,j+1))
 NNg[j+1] = 0+system(sprintf("grep N %s | awk 'NR==%d{print $4}'", fing,j+1))
 eccg[j+1] = 0+system(sprintf("grep ecc %s | awk 'NR==%d{print $4}'", fing,j+1))
}

set key t l 

k=lastkg
set ylabel 'angle w.r.t. $\{x >0\}$'
p [0:1] fing i k ev :::0::0 u ($0/NNg[k+1]):1 pt 7 ps .05 lc rgb 'red' w lp t 'center', '' i k ev :::0::0 u ($0/NNg[k+1]):2 pt 7 ps .05 lc rgb 'blue' w lp t 'stable'

set ylabel 'center - stable'
p [0:1] fing i k ev :::0::0 u ($0/NNg[k+1]):($1-$2) pt 7 ps .05 lc rgb 'black' w lp not

set key t c
set xlabel '$\theta$'
k=lastk
set ylabel 'angle w.r.t. $\{x >0\}$'
p [0:1] fin i k ev :::0::0 u ($0/NN[k+1]):1 pt 7 ps .05 lc rgb 'red' w lp t 'center', '' i k ev :::0::0 u ($0/NN[k+1]):2 pt 7 ps .05 lc rgb 'blue' w lp t 'stable'

set ylabel 'center - stable'
p [0:1] fin i k ev :::0::0 u ($0/NN[k+1]):($1-$2) pt 7 ps .05 lc rgb 'black' w lp not

unset multiplot
set output
