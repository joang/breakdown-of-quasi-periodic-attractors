set t epslatex nobackground size 7.75in,4in

if (! exists('fin1')) fin1='../data/tor3gold-77350-sob.txt'
if (! exists('fin2')) fin2='../data/tor3-77349-sob.txt'
if (! exists('fout')) fout='sob.tex'

set output fout
set multiplot layout 1,2
unset grid

set ylabel '$H _r$'

set ytics format '$10^{%T}$'
set logscale y
set key t l 

set title '$\omg _1$'
set xlabel '$\eps$'
p for[j=8:2:-1] fin1 u 1:j w l lw 2.5 dt j lc j t sprintf('\small $r=%d$',j)
unset ylabel

set title '$\omg _2$'
set xlabel '$\eps$'
p for[j=8:2:-1] fin2 u 1:j w l lw 2.5 dt j lc j t sprintf('\small $r=%d$',j)
unset multiplot
set output
