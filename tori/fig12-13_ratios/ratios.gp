set t epslatex monochrome nobackground size 7.75in,4in

if (! exists('fin1')) fin1='m5r3-tor3gold-80293-sob.txt'
if (! exists('fin3')) fin3='m4r6-tor3-80489-sob.txt'

if (! exists('fin1b')) fin1b='m5r3-tor3g-81475-sob.txt'
if (! exists('fin3b')) fin3b='m4r6-tor3-81183-sob.txt'

if (! exists('fout')) fout='ratios.tex'
if (! exists('fout2')) fout2='ratios3d.tex'

set output fout
set multiplot layout 1,2
unset grid

set title '$\omg _1$'
set logscale x
epsc1=system(sprintf("awk '{print $1}' %s | tail -n 1", fin1))+0
set xlabel '$(1-\eps/\eps _{\omg _1}^c)^{-1}$'
# set xlabel '$\eps _{\omg _1}$'
set ylabel '$H_1H_4/(H_2H_3)$'
p fin1 u (1./(1-$1/epsc1)):6 w lp pt 7 ps .6 not, fin1b u (1./(1-$1/epsc1)):6 w l not 

set title '$\omg _2$'
epsc3=system(sprintf("awk '{print $1}' %s | tail -n 1", fin3))+0
set xlabel '$(1-\eps/\eps _{\omg _2}^c)^{-1}$'
# set xlabel '$\eps _{\omg _2}$'
set ylabel '$H_1^{3}H_3^{2}H_4^{2}/(H_1H_2^{4}H_4^{2})$'
p fin3 u (1./(1-$1/epsc3)):1023 w lp pt 7 ps .6 not, fin3b u (1./(1-$1/epsc3)):1023 w l not

unset logscale x

unset multiplot
set output

set output fout2
set multiplot layout 1,2
set grid

unset title
set ytics autofreq 1


set xlabel '\scriptsize $H_1H_4/(H_2H_3)$' rotate parallel
set ylabel '\scriptsize $H_1H_2H_5^{2}/(H_2^{2}H_4H_5)$' font ",8" rotate parallel
set zlabel '\scriptsize $H_1H_3H_4^{2}H_5^{2}/(H_2^{2}H_4^{2}H_5^{2})$' rotate parallel
sp fin1 u 6:194:335 w lp pt 7 ps .6 not, fin1b u 6:194:335 w l not

set ytics autofreq .4
set xlabel '\scriptsize $H_1^{5}H_4^{2}/(H_1^{4}H_2H_3H_4)$' rotate parallel
set ylabel '\scriptsize $H_1^{4}H_3/(H_1^{3}H_2^{2})$' rotate parallel
set zlabel '\scriptsize $H_1^{3}H_3^{2}H_4^{2}/(H_1H_2^{4}H_4^{2})$' font ",8" rotate parallel
sp fin3 u 671:5:1023 w lp pt 7 ps .6 not, fin3b u 671:5:1023 w l not

unset multiplot
set output
