#include <stdio.h>
#include <stdlib.h>
#include <mpfr.h>
#include <assert.h>
#include <string.h>


#define alloc(n,x) x = (__typeof__(x)) malloc((n)*sizeof(*(x))), assert(x)
#define dealloc(x) if (x) free(x), x=NULL

#define MYPREC 70
#define REAL mpfr_t
#define MXSTR 1024

typedef struct
{
  unsigned m, r, N;
  unsigned **n, **d;
} scalings_t;

static FILE *file=NULL;
static char filename[MXSTR];

static int nextline(FILE *file)
{
  char c;
  while ((c=fgetc(file))!=EOF) if (c=='\n') return 1;
  return 0;
}

static void print_usage(char *fname)
{
  printf("> .%s <file1> <file2> <file3> \n"
         "\t file1: Filename of orders \n"
         "\t file2: Filename of data \n"
         "\t file3: Filename of plot \n", fname);
}

int main(int argc, char *argv[])
{
  unsigned i, j, k, nrows, ncols;
  int l;
  REAL **expos=NULL, *epsis=NULL, nume, deno;
  scalings_t s;
  
  mpfr_set_default_prec(MYPREC);
  mpfr_set_default_rounding_mode(MPFR_RNDN);
  
  if (argc < 3 || strncmp("-h", argv[1], 3*sizeof(char)) == 0) { print_usage(argv[0]); exit(1); }

  file = fopen(argv[1], "r"); assert(file);
  printf("File: %s \n", argv[1]);

  l = fscanf(file, "%u %u %u ", &s.N, &s.m, &s.r); assert(l==3);
  printf("\tN=%u m=%u r=%u\n", s.N, s.m, s.r);
  alloc(s.N, s.n); alloc(s.N*s.m, *s.n);
  alloc(s.N, s.d); alloc(s.N*s.m, *s.d);

  for (k = 0; k < s.N; ++k)
    {
      s.n[k] = &s.n[0][k*s.m];
      s.d[k] = &s.d[0][k*s.m];
      for (j = 0; j < s.m; ++j) {l=fscanf(file, "%u ", &s.n[k][j]); assert(l==1);}
      for (j = 0; j < s.m; ++j) {l=fscanf(file, "%u ", &s.d[k][j]); assert(l==1);}
    }

  fclose(file); file=NULL;

  file = fopen(argv[2], "r"); assert(file);
  printf("File: %s \n", argv[2]);

  char str[MXSTR], *pstr=NULL;
  
  nrows=0; 
  while ((*str=fgetc(file))!=EOF) {if (*str=='#') nextline(file); else if (*str=='\n') nrows++;}
  rewind(file);
  ncols=0; while ((*str=fgetc(file))!=EOF) {if (*str==' ') ncols++; else if (*str=='\n') break;} if (ncols) ncols-= 2;
  rewind(file);
  nextline(file);
  printf("\tnrows=%u ncols=%u\n", nrows, ncols);
  
  /* memory allocation */
  alloc(nrows, epsis);
  alloc(nrows, expos);
  for (k = 0; k < nrows; ++k) {
    mpfr_init2(epsis[k],mpfr_get_default_prec());
    alloc(ncols, expos[k]);
    for (j = 0; j < ncols; ++j) mpfr_init2(expos[k][j],mpfr_get_default_prec());
  }
  /* reading */
  for (k = 0; k < nrows; ++k)
    {
      mpfr_inp_str(epsis[k], file, 10, mpfr_get_default_rounding_mode());
      for (j = 0; j < ncols; ++j)
        {
          mpfr_inp_str(expos[k][j], file, 10, mpfr_get_default_rounding_mode());
        }
    }

  fclose(file); file=NULL;
  /* END reading datafile */


  snprintf(filename, sizeof(char)*MXSTR, "%s", argv[3]);
  printf("File: %s \n", argv[3]);

  assert(s.m <= ncols);
  file = fopen(filename, "w"); assert(file);
  mpfr_init2(nume,mpfr_get_default_prec());
  mpfr_init2(deno,mpfr_get_default_prec());
  for (k = 0; k < nrows; ++k)
    {
      mpfr_fprintf(file, "% .20RNe ",epsis[k]);

      for (i = 0; i < s.N; ++i)
        {
          mpfr_set_ui(nume,1,mpfr_get_default_rounding_mode());
          for (j = 0; j < s.m; ++j) {
            l=s.n[i][j]; l-=s.d[i][j]; if (l==0) continue;
            mpfr_pow_si(deno,expos[k][j],l,mpfr_get_default_rounding_mode());
            mpfr_mul(nume,nume,deno,mpfr_get_default_rounding_mode());
          }
          mpfr_fprintf(file,"% .20RNe ",nume);
        }
      fprintf(file, "\n");
    }
  mpfr_clear(deno); mpfr_clear(nume);
  fclose(file); file=NULL;


  snprintf(str, sizeof(char)*MXSTR, "%s.gp", argv[3]);
  file = fopen(str, "w"); assert(file);

  fprintf(file, "set t pdf enhanced size 4in,4in\nset output '%s.pdf'\n", filename);

  fprintf(file, "st '%s' u 1 noout\n", filename);
// #define DEFAULT
#ifndef DEFAULT
  fprintf(file, "epsi=system(\"awk '{print $1}' %s | tail -n 1\")+0\n",filename);
  fprintf(file, "set logscale x\n"); 
  fprintf(file, "set xlabel '(1-{/Symbol e}/{/Symbol e}^c)^{-1}'\n");
#else
  fprintf(file, "set xlabel '{/Symbol e}'\n");
#endif
  
  for (i = 0; i < s.N; ++i)
    {
      fprintf(file, "\nset ylabel '");
      for (j = 0; j < s.m; ++j)
        {
          if (s.n[i][j] == 1) fprintf(file, "H_%u", j+1);
          else if (s.n[i][j] > 1) fprintf(file, "H_%u^{%u}", j+1, s.n[i][j]);
        }
      fprintf(file, "/(");
      for (j = 0; j < s.m; ++j)
        {
          if (s.d[i][j] == 1) fprintf(file, "H_%u", j+1);
          else if (s.d[i][j] > 1) fprintf(file, "H_%u^{%u}", j+1, s.d[i][j]);
        }
      fprintf(file, ")'\n");

      
#ifndef DEFAULT
      fprintf(file, "p '%s' u (1/(1-$1/epsi)):%d w lp pt 7 ps .3 not\n", filename, i+2);
#else 
      fprintf(file, "p '%s' u 1:%d w lp pt 7 ps .3 not\n", filename, i+2);      
#endif
    }
  fprintf(file, "\nset output\n");

  fclose(file); file=NULL;
  
  printf("gnuplot %s\n", str);

  goto end;
end:
  for (k = 0; k < nrows; ++k) 
  {
    for (j = 0; j < ncols; ++j) mpfr_clear(expos[k][j]);
    mpfr_clear(epsis[k]);
  }
  for (k = 0; k < nrows; ++k) dealloc(expos[k]);
  dealloc(expos);
  dealloc(epsis);
  dealloc(*s.d); dealloc(s.d);
  dealloc(*s.n); dealloc(s.n);
  mpfr_free_cache();
  return 0;
}
