#!/bin/sh

order=${1:-m3r4}
exe=./plot_q.exe

gcc plot_q.c -lgmp -lmpfr -lm -o plot_q.exe

for file in ../data/tor3-80489-sob.txt ../data/tor3avg-80620-sob.txt ../data/tor3gold-80293-sob.txt ../data/tor3gavg-80372-sob.txt
do 
  echo $file done
  out=$order-$file
  rm $out
  $exe $order $file $out
  gnuplot $out.gp
  cd -
done
