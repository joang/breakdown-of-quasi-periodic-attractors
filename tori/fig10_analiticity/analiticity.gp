set t epslatex nobackground size 7.75in,4in

if (! exists('fing')) fing='../data/tor3gold-77350-ang.txt'
if (! exists('fin')) fin='../data/tor3-77349-ang.txt'

if (! exists('finpg')) finpg='../data/tor3gold-77350-fou.txt'
if (! exists('finp')) finp='../data/tor3-77349-fou.txt'
if (! exists('fout')) fout='analiticity.tex'

f(x) = a*x+b

set output fout
set multiplot layout 1,2
unset grid

st fin u 1 noout
lastk = STATS_blocks-2;
array ecc[STATS_blocks-1];
array epsi[STATS_blocks-1];
array NN[STATS_blocks-1];
array slope_diff[STATS_blocks-1];
array slope_diffp1[STATS_blocks-1];
array slope_diffp2[STATS_blocks-1];

array epsi_4[4];
array slope_diff_4[4];
array slope_diffp1_4[4];
array slope_diffp2_4[4];

do for [j=0:(STATS_blocks-2)] {
 epsi[j+1] = 0+system(sprintf("grep eps %s | awk 'NR==%d{print $4}'", fin,j+1))
 NN[j+1] = 0+system(sprintf("grep N %s | awk 'NR==%d{print $4}'", fin,j+1))
 ecc[j+1] = 0+system(sprintf("grep ecc %s | awk 'NR==%d{print $4}'", fin,j+1))
 
fit f(x) fin i j ev 2:::1::1 u ($0*2):(log10(sqrt(($1-$4)**2+($2-$3)**2))>-30?log10(sqrt(($1-$4)**2+($2-$3)**2)):1/0) via a,b
slope_diff[j+1] = a

fit f(x) finp i j ev 2:::0::0 u ($0*2):(log10($3)>-30?log10($3):1/0) via a,b
slope_diffp1[j+1] = a
fit f(x) finp i j ev 2:::0::0 u ($0*2):(log10($6)>-30?log10($6):1/0) via a,b
slope_diffp2[j+1] = a
}
do for [j=0:3] {
  epsi_4[j+1] = epsi[STATS_blocks-2-(j+1)]; 
  slope_diff_4[j+1] = slope_diff[STATS_blocks-2-(j+1)]; 
  slope_diffp1_4[j+1] = slope_diffp1[STATS_blocks-2-(j+1)]; 
  slope_diffp2_4[j+1] = slope_diffp2[STATS_blocks-2-(j+1)]; 
}

st fing u 1 noout
lastkg = STATS_blocks-2;
array eccg[STATS_blocks-1];
array epsig[STATS_blocks-1];
array NNg[STATS_blocks-1];
array slope_diffg[STATS_blocks-1];
array slope_diffgp1[STATS_blocks-1];
array slope_diffgp2[STATS_blocks-1];

array epsig_4[4];
array slope_diffg_4[4];
array slope_diffgp1_4[4];
array slope_diffgp2_4[4];

do for [j=0:(STATS_blocks-2)] {
 epsig[j+1] = 0+system(sprintf("grep eps %s | awk 'NR==%d{print $4}'", fing,j+1))
 NNg[j+1] = 0+system(sprintf("grep N %s | awk 'NR==%d{print $4}'", fing,j+1))
 eccg[j+1] = 0+system(sprintf("grep ecc %s | awk 'NR==%d{print $4}'", fing,j+1))
 
fit f(x) fing i j ev 2:::1::1 u ($0*2):(log10(sqrt(($1-$4)**2+($2-$3)**2))>-30?log10(sqrt(($1-$4)**2+($2-$3)**2)):1/0) via a,b
slope_diffg[j+1] = a


fit f(x) finpg i j ev 2:::0::0 u ($0*2):(log10($3)>-30?log10($3):1/0) via a,b
slope_diffgp1[j+1] = a
fit f(x) finpg i j ev 2:::0::0 u ($0*2):(log10($6)>-30?log10($6):1/0) via a,b
slope_diffgp2[j+1] = a
}

do for [j=0:3] {
  epsig_4[j+1] = epsig[STATS_blocks-2-(j+1)]; 
  slope_diffg_4[j+1] = slope_diffg[STATS_blocks-2-(j+1)]; 
  slope_diffgp1_4[j+1] = slope_diffgp1[STATS_blocks-2-(j+1)]; 
  slope_diffgp2_4[j+1] = slope_diffgp2[STATS_blocks-2-(j+1)]; 
}

set key t r 

# set title offset 0,-.5
set title '$\omg _1$'
set ylabel 'analiticity domain'
set xlabel '$\eps$'
p epsig u (epsig[$1]):((0-slope_diffg[$1]*log(10))/(2*pi)) w lp pt 7 ps .05 lc rgb 'black' t 'center-stable', '' u (epsig[$1]):((0-slope_diffgp1[$1]*log(10))/(2*pi)) w lp lc rgb 'red' pt 7 ps .05  t '$u$'
#, '' u (epsig[$1]):((0-slope_diffgp2[$1]*log(10))/(2*pi)) w lp lc rgb 'blue' pt 7 ps .05 t 'coord. 2'
unset ylabel

set title '$\omg _2$'
set xlabel '$\eps$'
p epsi u (epsi[$1]):((0-slope_diff[$1]*log(10))/(2*pi)) w lp pt 7 ps .05 lc rgb 'black' t 'center-stable', '' u (epsi[$1]):((0-slope_diffp1[$1]*log(10))/(2*pi)) w lp lc rgb 'red' pt 7 ps .05 t '$u$'

unset title
unset xlabel
set size square .5,.5
set origin 0.071,0.3
set ytics autofreq .002 font ',7'
set xtics autofreq .00025 rotate by -90 font ',7'
p epsig_4 u (epsig_4[$1]):((0-slope_diffg_4[$1]*log(10))/(2*pi)) w lp pt 7 ps .05 lc rgb 'black' not, '' u (epsig_4[$1]):((0-slope_diffgp1_4[$1]*log(10))/(2*pi)) w lp lc rgb 'red' pt 7 ps .05 not

set size square .5,.5
# set origin 0.101,0.44
set origin 0.571,0.3
set xtics rotate by -90 font ',7'
p epsi_4 u (epsi_4[$1]):((0-slope_diff_4[$1]*log(10))/(2*pi)) w lp pt 7 ps .05 lc rgb 'black' not, '' u (epsi_4[$1]):((0-slope_diffp1_4[$1]*log(10))/(2*pi)) w lp lc rgb 'red' pt 7 ps .05 not

unset multiplot
set output
