#!/bin/bash 
# nohup sh run.sh rot-line-0.25 1e-4 2 1 0.25 2 1 1e-8 2 1 0 2 1 0 1e-2 1 4 &
folder=${1:-rot}
mkdir -p $folder
epsa=${2:-1e-4}
epsh=${3:-1e-5}
epsb=${4:-4.5e-4}

ecca=${5:-0.0549}
ecch=${6:-1e-4}
eccb=${7:-0.056}

disa=${8:-1e-8}
dish=${9:-1e-8}
disb=${10:-3e-7}


x1a=${11:-0}
x1h=${12:-1}
x1b=${13:-0.5}

x2a=${14:-0}
x2h=${15:-1e-2}
x2b=${16:-0}


lessproc=${17:-4}

epsrange=`awk -v a=$epsa -v h=$epsh -v b=$epsb 'BEGIN{for(i=a; i<=b; i+=h) print i}'`
eccrange=`awk -v a=$ecca -v h=$ecch -v b=$eccb 'BEGIN{for(i=a; i<=b; i+=h) print i}'`
disrange=`awk -v a=$disa -v h=$dish -v b=$disb 'BEGIN{for(i=a; i<=b; i+=h) print i}'`
x1range=`awk -v a=$x1a -v h=$x1h -v b=$x1b 'BEGIN{for(i=a; i<=b; i+=h) print i}'`
x2range=`awk -v a=$x2a -v h=$x2h -v b=$x2b 'BEGIN{for(i=a; i<=b; i+=h) print i}'`

make
exe=./main

Nmin=5400
Nmax=5405
Ntrans=100000000
nproc=`nproc --all`
nproc=$((nproc-$lessproc))
echo nproc=$nproc
commands() {
  local ofile=${1:-$folder-cmds.txt}
  rm -f $ofile
  for x1 in $x1range
  do
  for x2 in $x2range
  do
    for dis in $disrange
    do
      for ecc in $eccrange
      do
        Ntrans=`awk -v e=$ecc -v Kd=$dis -v tol=1e-14 \
        'BEGIN{printf("%d", \
        1.1*log(tol)/(-Kd*atan2(0,-1)*(3*e^4+24*e^2+8)/(4*(1-e*e)^4.5)))}'`
        for eps in $epsrange
        do
          out=$folder/rot-$eps-$ecc-$dis-$x1-$x2.txt
          echo "timeout 4d $exe $out $eps $ecc $dis $Nmin $Nmax $Ntrans $x1 $x2 > /dev/null" >> $ofile
        done
      done
    done
  done
  done
}
commands 
