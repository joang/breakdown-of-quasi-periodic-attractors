#ifndef TYPES_H
#define TYPES_H

#define LNI  long int
#define REAL double
#define SCAL double
#define S(x) x
#define L(x) x
#define R(x) x
#define BITS_PREC 135
#define LOG10ABSERR -16
#define LOG10RELERR -16
#define RFMT "% .16le"
#define LFMT "%ld"
#define SFMT "% .16le"
#define RIFMT "%lf"
#define LIFMT "%ld"
#define SIFMT "% le"

#endif // TYPES_H
