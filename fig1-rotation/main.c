/**
 * @brief
 *  .
 * @author J. Gimeno (BGSMath-UB & GT & uniroma2) <joan@maia.ub.es>
 * @date 2019-05-24
 *       2020-02-17 new version
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <assert.h>

#include "taylor.h"
#include "types.h"

/* === EASY_C_OVERLOAD === */
#define init(x)        {}
#define clear(x)       {}
/* assigns */
#define set(x,a)       (x) = (a)
#define set_zero(x)    set(x,0)
#define set_ui(x,a)    set(x,a)
#define set_str(x,s)   set(x,atof(s))

/* ternary arithmetic operations */
#define add2(x,a,b)    (x) = (a) + (b)
#define add2_ui(x,a,b) (x) = (a) + (b)
#define sub2(x,a,b)    (x) = (a) - (b)
#define mul2(x,a,b)    (x) = (a) * (b)
#define div2(x,a,b)    (x) = (a) / (b)
#define sub2_ui(x,a,b) (x) = (a) - (b)
#define mul2_scal(x,a,b) (x) = (a) * (b)
#define mul2_d(x,a,b)    (x) = (a) * (b)
#define mul2_ui(x,a,b)   (x) = (a) * (b)
#define mul2_si(x,a,b)   (x) = (a) * (b)
#define mul2_2ui(x,a,b)  (x) = (a) * (1 << (b))
#define div2_2ui(x,a,b)  (x) = (a) / (1 << (b))
#define ui_sub2(x,a,b)   (x) = (a) - (b)

/* binary arithmetic operations */
#define add(x,a)         (x)+= (a)
#define sub(x,a)         (x)-= (a)
#define mul(x,a)         (x)*= (a)
#define div(x,a)         (x)/= (a)
#define add_d(x,a)       (x)+= (a)
#define sub_d(x,a)       (x)-= (a)
#define mul_d(x,a)       (x)*= (a)
#define div_d(x,a)       (x)/= (a)
#define add_ui(x,a)      (x)+= (a)
#define sub_ui(x,a)      (x)-= (a)
#define mul_ui(x,a)      (x)*= (a)
#define div_ui(x,a)      (x)/= (a)
#define div_si(x,a)      (x)/= (a)
#define mul_2ui(x,a)     (x)*= (1 << (a))
#define div_2ui(x,a)     (x)/= (1 << (a))
#define sub_scal(x,a)    (x)-= (a)
#define mul_scal(x,a)    (x)*= (a)
#define div_scal(x,a)    (x)/= (a)
#define ui_div(a,x)      (x) = (a) / (x)
#define ui_div_si(a,x)   (x) = (a) / (x)

/* unary arithmetic operations */
#define neg(x,a)         set(x,-(a))

/* special functions */
#define set_sqrt(x,a)     set(x,sqrt(a))
#define set_sin(x,a)      set(x,sin(a))
#define set_cos(x,a)      set(x,cos(a))
#define set_tan(x,a)      set(x,tan(a))
#define set_abs(x,a)      set(x,fabs(a))
#define set_pow(x,a,b)    set(x,pow(a,b))
#define nrminf(e,x)       set_abs(*(e),x)
#define set_exp(x,a)      set_abs(x,exp(a))
#define set_round(x,a)    set(x,round(a))

/* boolean relations */
#define equal_p(x,y)      (x) == (y)
#define greater_p(x,y)    (x) > (y)
#define less_p(x,y)       (x) < (y)
#define lessequal_p(x,y)  (x) <= (y)
#define cmp(x,y)          (equal_p(x,y) ? 0 : (less_p(x,y) ? -1 : 1))
#define cmpabs(x,y)       cmp(fabs(x), fabs(y))
#define cmp_d(x,y)        cmp(x, y)
#define cmp_ui(x,y)       cmp(x, y)

/* castings */
#define set_pi(x)         set(x,M_PI)
/* === END EASY_C_OVERLOAD === */

#define alloc(n,x) x=(__typeof__(x)) malloc((n)*sizeof(*(x)))
#define dealloc(x) free(x), x = NULL

/* TAYLOR integration */
MY_FLOAT *x=NULL;
MY_FLOAT t, ht, tf;
double log10abs=LOG10ABSERR, log10rel=LOG10RELERR;
int dir=+1, step_ctl=2, order;
/* end TAYLOR integration */

/* TAYLOR global variables */
MY_FLOAT ecc, eps, dis, omg1;
/* end TAYLOR global variables */

static FILE *file=NULL;
static clock_t cputime;

static REAL pi;

#define WEIGHT 2
static
void phi(SCAL p[1], SCAL t)
{
  if (S(cmp_ui)(t, 0) > 0 && S(cmp_ui)(t, 1) < 0)
    {
#if WEIGHT==0
      S(set_ui)(p[0], 1);
#elif WEIGHT==1
      S(sub2_ui)(p[0], t, 1);
      S(mul)(p[0], t);
      S(ui_div_si)(1, p[0]);
      S(set_exp)(p[0], p[0]);
#elif WEIGHT==2
      // p <- exp(-1/(t^2(t-1)^2))
      S(sub2_ui)(p[0], t, 1);
      S(mul)(p[0], p[0]);
      S(mul)(p[0], t);
      S(mul)(p[0], t);
      S(ui_div_si)(-1, p[0]);
      S(set_exp)(p[0], p[0]);
#elif WEIGHT==3
      S(set_pi)(p[0]);
      S(mul)(p[0], t);
      S(set_sin)(p[0], p[0]);
      S(mul)(p[0], p[0]);
#endif
    }
  else S(set_str)(p[0], "0.0");
}

static
int test(const char *fileout,
          REAL epsi, REAL dissi, REAL ecci,
          LNI Nmin, LNI Nmax, LNI Ntrans,
          REAL *xinit)
{
  double minx0=1.0, maxx0=-1.0;
  LNI N, j, i, d=2;

  REAL normalisation, weight;
  REAL err, tol_rot;
  REAL rotold, rot;
  REAL *orbit=NULL;

  alloc(Nmax+1, orbit);
  for (j = 0; j <= Nmax; ++j) R(init)(orbit[j]);
  alloc(d, x);
  for (i = 0; i < d; ++i) R(init)(x[i]);
  S(init)(t);
  S(init)(ht);
  S(init)(tf);
  S(init)(pi); S(set_pi)(pi);

  R(init)(tol_rot); R(set_str)(tol_rot, "1e-10");
  R(init)(err);
  R(init)(rotold);
  R(init)(rot); R(set_str)(rot, "0");
  R(init)(normalisation);
  R(init)(weight);

  R(init)(eps); R(set)(eps, epsi);
  R(init)(ecc); R(set)(ecc, ecci);
  R(init)(dis); R(set)(dis, dissi);
  R(init)(omg1); R(set_str)(omg1, "1.0e0");


  file = fopen(fileout, "a");
  R(fprintf)(file, "# eps = " RFMT "\n", eps);
  R(fprintf)(file, "# dis = " RFMT "\n", dis);
  R(fprintf)(file, "# ecc = " RFMT "\n", ecc);
  L(fprintf)(file, "# Nmin = " LFMT "\n", Nmin);
  L(fprintf)(file, "# Nmax = " LFMT "\n", Nmax);
  L(fprintf)(file, "# Ntrans = " LFMT "\n", Ntrans);
  fclose(file); file = NULL;
  R(set)(x[0], xinit[0]);
  R(set)(x[1], xinit[1]);
  S(mul2_2ui)(tf, pi, 1);

  printf("==== INPUT ====\n");
  printf("fileout=" "%s" "\n", fileout);
  R(printf)("Nmin=" LFMT "\n", Nmin);
  R(printf)("Nmax=" LFMT "\n", Nmax);
  R(printf)("Ntrans= " LFMT "\n", Ntrans);
  R(printf)("tolrot=" RFMT "\n", tol_rot);
  R(printf)("omg1=" RFMT "\n", omg1);
  R(printf)("eps=" RFMT "\n", eps);
  R(printf)("dis=" RFMT "\n", dis);
  R(printf)("ecc=" RFMT "\n", ecc);
  printf("==== ===== ====\n");
  printf("log10abs=%g\n", log10abs);
  printf("log10rel=%g\n", log10rel);
  printf("==== ===== ====\n");
  for (i = 0; i < d; ++i) R(printf)("x%d=" RFMT "\n", i, x[i]);
  printf("==== ===== ====\n\n");


  for (i = 0; i < d; ++i) R(mul_scal)(x[i], pi);
  R(ui_sub2)(ht, 1, ecc);
  R(mul)(x[1], ht);
  cputime = clock();
  for (j = 0; j < Ntrans; ++j)
    {
      R(set_str)(t, "+0.0");
      while (taylor_step_ode(&t, x, dir, step_ctl, log10abs, log10rel, &tf, &ht, &order,NULL) != 1);
//      x[0] = (x[0]/pi - round(x[0]/pi))*pi;
      R(div_scal)(x[0], pi);
      R(set_round)(ht, x[0]);
      R(sub_scal)(x[0], ht);
      R(mul_scal)(x[0], pi);
    }
  cputime = clock() - cputime;
  for (i = 0; i < d; ++i) R(div_scal)(x[i], pi);
  R(ui_sub2)(ht, 1, ecc);
  R(div)(x[1], ht);

  file = fopen(fileout, "a");
  fprintf(file, "# cpu-time=% .10e \n", ((double)(cputime))/CLOCKS_PER_SEC);
  R(fprintf)(file, RFMT " ", ht);
  for (i = 0; i < d; ++i) R(fprintf)(file, RFMT " ", x[i]); fprintf(file, "\n");
  fclose(file); file = NULL;

  file = fopen(fileout, "a");
  cputime = clock();
  for (j = 0; j <= Nmax; ++j)
    {
      R(set_str)(t, "+0.0");
      R(set)(orbit[j], x[1]); // gamma

      if (x[0] < minx0) minx0 = x[0];
      if (maxx0 < x[0]) maxx0 = x[0];

      for (i = 0; i < d; ++i) R(mul_scal)(x[i], pi);
      R(ui_sub2)(ht, 1, ecc);
      R(mul)(x[1], ht);
      while (taylor_step_ode(&t, x, dir, step_ctl, log10abs, log10rel, &tf, &ht, &order,NULL) != 1);
      for (i = 0; i < d; ++i) R(div_scal)(x[i], pi);
      R(ui_sub2)(ht, 1, ecc);
      R(div)(x[1], ht);

      R(set_round)(ht, round(x[0]));
      R(sub)(x[0], ht);
      R(fprintf)(file, RFMT " ", ht);
      R(fprintf)(file, RFMT " ", x[0]);
      R(fprintf)(file, RFMT " ", x[1]);
      fprintf(file, "\n");
    }
  fprintf(file, "# cpu-time=% .10le \n\n\n", ((double)(clock()-cputime))/CLOCKS_PER_SEC);
  fclose(file); file = NULL;

  R(set_str)(rot, "+0.0");
  for (N = Nmin; N <= Nmax; N+= 10)
    {
      R(set)(rotold, rot);
      R(set_str)(rot, "+0.0");
      R(set_str)(normalisation, "+0.0");

      for (j = 1; j <= N; ++j)
        {
          S(set_ui)(weight, j);
          S(div_ui)(weight, N); // weight <- j/N
          phi(&ht, weight); // ht <- 1
          R(set)(weight, orbit[j]); // weight <- orbit[j]
//          R(sub2)(weight, orbit[j], orbit[j-1]);
          R(mul)(weight, ht);
          R(add)(rot, weight);
          R(add)(normalisation, ht);
        }
      R(div)(rot, normalisation);
//      ht = round(rot);
//      rot-= ht;
      R(sub2)(err, rotold, rot); R(set_abs)(err, err);
      R(printf)(RFMT " ", err);
      R(printf)("%g ", ht);
      R(printf)(RFMT " ", rot);
      L(printf)(LFMT "\n", N);
      if (Nmin < N)
        {
          file = fopen(fileout, "a");
          R(fprintf)(file, "%g ", ht);
          R(fprintf)(file, RFMT " ", rot);
          R(fprintf)(file, RFMT " ", err);
          L(fprintf)(file, LFMT "\n", N-Nmin);
          fclose(file); file = NULL;

          if (R(less_p)(err, tol_rot))
            {
              R(printf)("rot=" RFMT "\n", rot);
              break;
            }
        }
    }
  cputime = clock()-cputime;

  file = fopen(fileout, "a");
  fprintf(file, "# cpu-time=% .10le \n", ((double)(cputime))/CLOCKS_PER_SEC);
  fclose(file); file = NULL;
  printf("# cpu-time=% .10le \n", ((double)(cputime))/CLOCKS_PER_SEC);

end:
  R(clear)(omg1);
  R(clear)(eps);
  R(clear)(ecc);
  R(clear)(dis);
  S(clear)(weight);
  S(clear)(normalisation);
  S(clear)(rot);
  S(clear)(rotold);
  S(clear)(err);
  R(clear)(tol_rot);

  S(clear)(pi);
  S(clear)(tf);
  S(clear)(ht);
  S(clear)(t);
  for (i = 0; i < d; ++i) R(clear)(x[i]); dealloc(x);
  for (j = 0; j <= Nmax; ++j) R(clear)(orbit[j]); dealloc(orbit);
//  printf("%e %e %e\n", maxx0, minx0, maxx0-minx0);
  return maxx0 - minx0 < 0.5;
}

static
void print_usage(void)
{
  printf("> ./main fileout eps diss ecc Nmin Nmax Ntrans x0 x1\n");
}

#define MXSTR 248
#define FILEOUT "rot.txt"
#define DIM 2
int main(int argc, const char *argv[])
{
  REAL epsi, ecci, dissi, xinit[DIM];
  LNI Nmin=4000, Nmax=4010,  Ntrans=10000;
  char fileout[MXSTR];
  int flag=0;

  print_usage();
  
  if (argc > 1 && strcmp(argv[1], "-h") == 0) return 0;

  R(init)(epsi);
  R(init)(ecci);
  R(init)(dissi);
  R(init)(x1);
  R(init)(x2);
  L(init)(Nmin);
  L(init)(Nmax);
  if (argc > 1) snprintf(fileout, MXSTR, "%s", argv[1]);
  else snprintf(fileout, MXSTR, "%s", FILEOUT);
  file = fopen(fileout, "w"); fclose(file); file = NULL;
  if (argc > 2) R(sscanf)(argv[2], "%lf", &epsi);
  else R(set_str)(epsi, "+0.0");
  if (argc > 4) R(sscanf)(argv[3], "%lf", &dissi);
  else R(set_str)(dissi, "1e-8");
  if (argc > 3) R(sscanf)(argv[4], "%lf", &ecci);
  else R(set_str)(ecci, "0.0549");
  if (argc > 5) L(sscanf)(argv[5], "%ld", &Nmin);
  else L(set)(Nmin, 3000);
  if (argc > 6) L(sscanf)(argv[6], "%ld", &Nmax);
  else L(set)(Nmax, 3005);
  if (argc > 7) L(sscanf)(argv[7], "%ld", &Ntrans);
  else L(set)(Ntrans, 10000);
  if (argc > 8) L(sscanf)(argv[8], "%lf", &xinit[0]);
  else R(set_str)(xinit[0], "+0.0");
  if (argc > 9) L(sscanf)(argv[9], "%lf", &xinit[1]);
  else R(set_str)(xinit[1], "+0.0");

  flag=test(fileout, epsi, dissi, ecci, Nmin, Nmax, Ntrans, xinit);

  L(clear)(Nmax);
  L(clear)(Nmin);
  R(clear)(dissi);
  R(clear)(ecci);
  R(clear)(epsi);
  R(clear)(x2);
  R(clear)(x1);
  return flag;
}
