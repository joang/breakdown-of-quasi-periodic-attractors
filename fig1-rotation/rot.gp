set t epslatex monochrome nobackground size 6in,3.5in

if (!exists('fin')) fin='values-rot.txt'
set output 'rotvsecc.tex'
unset grid

set key out t r L rev width -5 spacing 2 
NL(ecc) = (1-ecc**2)**(-3./2)*(1 + 15.0*ecc**2/2 + 45.0*ecc**4/8 + 5.0*ecc**6/16)/ (1 + 3.0 * ecc**2 + 3.0 * ecc**4/8) 
set multiplot
set xlabel '$\ecc$'
set ylabel 'rotation number'
set size square 1,1
set origin 0, 0
p [:.5] fin u 5:($1*pi) w p lw 1.2 lc 0 not,(sqrt(5)+1)/2 t '\small $\omg _1$', 1 + 1 / (2 + (sqrt(5)-1)/2) t '\small $\omg _2$', NL(x) t '\small $\bar N(\ecc)/\bar L(\ecc)$'
unset xlabel
unset ylabel
set ytics autofreq .000002 font ',8' 
set xtics rotate by -90 1e-6 font ',8'
set size square .5,.5
set origin 0.101,0.44
p [.250205:.250209][1.381964:1.381970] fin u 5:($1*pi) w lp lw 1.2 lc 0 not, 1 + 1 / (2 + (sqrt(5)-1)/2) not, NL(x) not lt 4
unset multiplot
set output

exit
