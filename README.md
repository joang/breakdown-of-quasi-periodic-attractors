# Breakdown of Quasi Periodic Attractors

## Description

 The files in this project provides data and source code used in the preprint [Calleja et al. (2023)](https://arxiv.org/abs/2210.05796)

 It is the final paper in a long project, [Calleja et al. (2022a)](https://arxiv.org/abs/2106.09175) and [Calleja et al. (2022b)](https://arxiv.org/abs/2107.02853). It mainly reports on phenomena discovered. The basic algorithms were reported previously.

## Software dependencies

 The code has been implemented and run it in Gnuplot and GCC.

## Authors
 R. Calleja, A. Celletti, J. Gimeno, and R. de la Llave

