extern MY_FLOAT eps, dis, ecc;

su= sin(u);
cu= cos(u);

r = 1.0 - ecc * cu;
r2= r*r;
r5= r2*r2*r;

# cos(f) = (cos(u)-ecc) / r
# sin(f) = sqrt(1-ecc**2) sin(u) / r
#
# cos(2f) = 2 cos(f)**2 - 1
# sin(2f) = 2 sin(f) cos(f)
cue = cu - ecc;
ecc2= sqrt(1- ecc*ecc);

cf2= 2*cue*cue/r2-1;
sf2= 2*cue*ecc2*su / r2;

x2 = 2*x;
sx2 = sin(x2);
cx2 = cos(x2);

# sin(2(x-f)) = sin(2x) cos(2f) - cos(2x) sin(2f)
s = sx2*cf2 - cx2*sf2;

diff(x,u)=y;
diff(y,u)=y*ecc*su/r - eps*s/(r) - dis*(y - ecc2/(r)) / (r5);
