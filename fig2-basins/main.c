#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include <omp.h>

#include "spin.h"

#define alloc(n,x) x = (__typeof__(x)) malloc((n)*sizeof(*(x))), assert(x)
#define dealloc(x) if (x) free(x), x = NULL

#define param_t double
#define scal_t double
#define int_t int

#define LOG10ABS -20
#define LOG10REL LOG10ABS
param_t eps=1.4e-2,ecc=0.247,dis=1e-3;
scal_t ti,ht, *x=NULL,*x0=NULL;
int ord;
#pragma omp threadprivate(ti,ht,ord,x,x0)

static scal_t pi=M_PI, twopi=2*M_PI, period=M_PI+M_PI;
static FILE *file=NULL;
static int nproc=1;

#define WEIGHT 1
static void phi(scal_t p[1], scal_t t)
{
  if (0.0 < t && t < 1.0)
    {
#if WEIGHT==0
      p[0] = 1.0e0;
#elif WEIGHT==1
      p[0] = exp(1.0e0 / (t*(t-1.0)));
#elif WEIGHT==2
      p[0] = (t-1.0)*t;
      p[0] = exp(-1.0 / (p[0]*p[0]));
#elif WEIGHT==3
      p[0] = sin(pi*t);
      p[0]*= p[0];
#endif
    }
  else p[0] = 0.0e0;
}

static void xtob(scal_t *b, scal_t *x)
{
  b[0] = x[0]; b[1] = x[1] * (1.0 - ecc);
}
static void btox(scal_t *x, scal_t *b)
{
  x[0] = b[0]; x[1] = b[1] / (1.0 - ecc);
}

static void compute_rotation(scal_t rot[1], int_t nrot, scal_t *weights, scal_t *x)
{
  int i;
  scal_t rot1, rot2, norma1, norma2;

  rot1 = rot2 = norma1 = norma2 = 0.0e0;

  xtob(x, x);
  for (i = 0; i < nrot; ++i)
    {
      ti = 0.0;
      while (taylor_step_spin(&ti, x, 1, 2, LOG10ABS, LOG10REL, &twopi, &ht, &ord) != 1);
      x[0]/= period; x[0] = (x[0] - round(x[0]))*period;

      rot1+= weights[i] * x[1];
      rot2+= weights[2*i+1] * x[1];

      norma1+= weights[i];
      norma2+= weights[2*i+1];
    }
  for (i = nrot; i < 2*nrot; ++i)
    {
      ti = 0.0;
      while (taylor_step_spin(&ti, x, 1, 2, LOG10ABS, LOG10REL, &twopi, &ht, &ord) != 1);
      x[0]/= period; x[0] = (x[0] - round(x[0]))*period;

      rot1+= weights[i] * x[1];
      norma1+= weights[i];
    }
  btox(x, x);
  rot1/= norma1;
  rot2/= norma2;

//  printf("rot1=% 10e\nrot2=% 10e\nerr=% .15e\n", rot1, rot2, rot1-rot2);
  if (fabs(rot1-rot2) < 1e-6) *rot = rot1 / (1.0 - ecc);
  else *rot=-1; // non-convergence
}

static void transient(scal_t ntrans, scal_t *x)
{
  int i;

  xtob(x, x);
  for (i = 0; i < ntrans; ++i)
    {
      ti = 0.0;
      while (taylor_step_spin(&ti, x, 1, 2, LOG10ABS, LOG10REL, &twopi, &ht, &ord) != 1);
      x[0]/= period; x[0] = (x[0] - round(x[0]))*period;
    }
  btox(x, x);
}

static void compute_lambda(scal_t l[1])
{
  *l = exp(-pi * dis * ((3*ecc*ecc + 24)*ecc*ecc + 8) / (4*pow(1-ecc*ecc, 4.5)));
}

static void print_usage(void)
{printf("> ./a.out fname xi/pi nx xe/pi yi ny ye nrot ecc eps dis\n");}

int main(int argc, char *argv[])
{
  if (argc > 1 && strncmp(argv[1], "-h", 2) == 0) {print_usage(); return 1;}

  int_t i, j, d=2, nrot=500;
  int_t nx=500, ny=500;
  scal_t xi=0, xe=pi, yi=1, ye=2, ntrans;
  scal_t *weights=NULL, *rots=NULL;

#ifdef _OPENMP
  nproc = omp_get_max_threads();
  double cputime;
#else
  clock_t cputime;
#endif

  i = 1;
  i+= 1; if (argc > i) xi = atof(argv[i]);
  i+= 1; if (argc > i) nx = atoi(argv[i]);
  i+= 1; if (argc > i) xe = atof(argv[i]);
  i+= 1; if (argc > i) yi = atof(argv[i]);
  i+= 1; if (argc > i) ny = atoi(argv[i]);
  i+= 1; if (argc > i) ye = atof(argv[i]);
  i+= 1; if (argc > i) nrot = atoi(argv[i]);
  i+= 1; if (argc > i) ecc = atof(argv[i]);
  i+= 1; if (argc > i) eps = atof(argv[i]);
  i+= 1; if (argc > i) dis = atof(argv[i]);

  xi*=period; xe*=period;

  alloc(d*nproc, x);
  alloc((nx+1)*(ny+1), rots);

  alloc(nrot, weights);
  for (i = 0; i < nrot; ++i) phi(weights+i, (i+1.0)/nrot);

  compute_lambda(x);
  ntrans =-1.1 * 14 / log10(*x);

#pragma omp parallel private(i) copyin(x)
  {
#ifdef _OPENMP
    i = omp_get_thread_num();
#else
    i = 0;
#endif
    x = x + i*d;
  }

#ifdef _OPENMP
  cputime = omp_get_wtime();
#else
  cputime = clock();
#endif

#pragma omp parallel private(i,j)
#pragma omp single
  for (i = 0; i <= nx; ++i) for (j = 0; j <= ny; ++j)
    {
#pragma omp task
      {
      x[0] = xi + i*(xe-xi)/nx;
      x[1] = yi + j*(ye-yi)/ny;
      transient(ntrans, x);
      compute_rotation(&rots[i*(ny+1)+j], nrot/2, weights, x);
      }
    }
#ifdef _OPENMP
  cputime = omp_get_wtime() - cputime;
#else
  cputime = clock() - cputime;
#endif

  file = fopen(argv[1], "w"); assert(file);
  fprintf(file, "# eps= %g\n", eps);
  fprintf(file, "# ecc= %g\n", ecc);
  fprintf(file, "# dis= %g\n", dis);
  fprintf(file, "# x= %g (%d) %g\n", xi, nx, xe);
  fprintf(file, "# y= %g (%d) %g\n", yi, ny, ye);
  fprintf(file, "# ntrans= %g\n", ntrans);
#ifdef _OPENMP
  fprintf(file, "# wtime(%d)= %g sec\n", nproc, (double) cputime);
#else
  fprintf(file, "# cputime= %g sec\n", (double) cputime / CLOCKS_PER_SEC);
#endif
  for (i = 0; i <= nx; ++i)
    {
      for (j = 0; j <= ny; ++j)
        fprintf(file, "% .15e % .15e % .15e\n",
                xi + i*(xe-xi)/nx, yi + j*(ye-yi)/ny,
                rots[i*(ny+1)+j]);
      fprintf(file, "\n");
    }
  fclose(file);

  dealloc(weights);
  dealloc(rots); dealloc(x);
  return 0;
}
