 
set t pdf enhanced size 5in,5in

if (! exists('fin')) fin='basin-'.ecc.'-'.eps.'-'.dis.'.txt'
if (! exists('fout')) fout=fin.'.pdf'

print fin
print fout

set palette defined (1.1 "black", 1.2 "grey", 1.3 "blue", 1.4 "green", 1.5 "violet", 1.6 "red", 1.7 "orange")

set pm3d map
set output fout
set title sprintf("ecc=%s dis=%s\neps=%s\n", ecc, dis, eps)
set xlabel 'x/{/Symbol p}'
set ylabel 'y'
set cblabel "rotation number ('white' means no convergence)" rotate by -90

if (exists('fin2')) {
  sp [0:2] fin u ($1/pi):2:($3 < 0? 1/0:$3) not, fin2 u ($1*2):($2*2*pi):(1) w l lc 0 lw 2 t 'K({/Symbol q})'
} else {
  sp fin u ($1/pi):2:($3 < 0? 1/0:$3) not 
}
  
set output
