#!/bin/bash
#SBATCH -J basin
#SBATCH -o out/jout-%j_%x
#SBATCH -e out/jerr-%j_%x
#SBATCH -t 2-1
#SBATCH -c 143

if [ -n "$SLURM_CPUS_PER_TASK" ]
then 
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
fi

if [ -z "$OMP_NUM_THREADS" ]
then 
export OMP_NUM_THREADS=`nproc --all`
fi

echo "nproc=$OMP_NUM_THREADS"
# exit

eps=${1:-5e-2}
ecc=${2:-0.247}
dis=${3:-1e-3}

xi=${4:-0}
nx=${5:-500}
xe=${6:-1}

yi=${7:-1}
ny=${8:-500}
ye=${9:-2}

nrot=${10:-2000}
fout=out/basin-$ecc-$eps-$dis.txt
mkdir -p out

echo "./main.exe $fout $xi $nx $xe $yi $ny $ye $nrot $ecc $eps $dis"
./main.exe $fout $xi $nx $xe $yi $ny $ye $nrot $ecc $eps $dis

echo "gnuplot -e \"ecc='$ecc';eps='$eps';dis='$dis';fin='$fout'\" plot.gp"
echo "# gnuplot -e \"ecc='$ecc';eps='$eps';dis='$dis';fin='$fout'\" plot.gp" >> $fout
gnuplot -e "ecc='$ecc';eps='$eps';dis='$dis';fin='$fout'" plot.gp
